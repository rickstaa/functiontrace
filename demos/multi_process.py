#!/usr/bin/env python3
import time
import logging
from multiprocessing import Pool
import fib

work = (["A", 0.5], ["B", 0.2], ["C", 0.1], ["D", 0.3])


def work_log(work_data):
    print("Process %s waiting %s seconds" % (work_data[0], work_data[1]))
    time.sleep(int(work_data[1]))
    fib.fib(12)
    print("Process %s Finished." % work_data[0])


def pool_handler():
    p = Pool(2)
    p.map(work_log, work)


if __name__ == "__main__":
    logging.warning("Starting multiprocessing")
    pool_handler()
    logging.critical("Finished multiprocessing")
